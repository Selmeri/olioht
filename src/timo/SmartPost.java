/*
 * Elmer Häyrynen, op.nro 0455933, c 2017
 */
package timo;

/**
 * Default class for smartposts, used by the XMLReader class. Implements also the Coordinates.java 
 * Includes all attributes related to smart posts e.g. addresses, and has functions for returning each of these
 * values to be used by the main program.
 */
public class SmartPost {
    private final String zip;
    private final String city;
    private final String address;
    private final String open;
    private final String postoffice;
    private final int id;
    Coordinates coordinates;  
    
    public SmartPost(String a, String b, String c, String d, String e, String f, String g, int h) {
        zip = a;
        city = b;
        address = c;
        open = d;
        postoffice = e;
        coordinates = new Coordinates(f, g);
        id = h;
    }    
    
    @Override
    public String toString() {
        return postoffice + ", " + address;
    }
    
    public String getCity() {
        return city;
    }
    
    public String getZIP() {
        return zip;
    }
    
    public String getAddress() {
        return address;
    }
    
    public String getOpen() {
        return open;
    }
    
    public String getPostoffice() {
        return postoffice;
    }
    
    public double getLatitude() {
        return coordinates.getLatitude();
    }
    
    public double getLongtitude() {
        return coordinates.getLongtitude();
    }
    
    public int getId() {
        return id;
    }    
}
