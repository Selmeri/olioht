/*
 * Elmer Häyrynen, op.nro 0455933, c 2017
 */
package timo;

public class Anvil extends Product {
    public Anvil(){
        super();
        name = "Anvil";
        fragile = false;
        dimension.put("weight", 200.0);
        dimension.put("height", 1.0);
        dimension.put("width", 1.0);
        dimension.put("depth", 1.0);
    }
    
}