/*
 * Elmer Häyrynen, op.nro 0455933, c 2017
 */
package timo;

import java.util.logging.Level;
import org.w3c.dom.Element;
import java.io.StringReader;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import org.xml.sax.InputSource;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import java.net.MalformedURLException;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStreamReader;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import java.net.URL;


/**
 * Loads the xml data from provided source, and initializes the smart posts based on the data.
 * Using SAX and Java.xml.Parsers.DocumentBuilderFactory for factory API for parsing DOM object trees from XML
 * 
 */
public class XMLReader {
    
    private final Document document;
    public SmartPost smartPost;
    private final DataBuilder smartPosts = DataBuilder.getInstance();
    static private XMLReader XMLR = null;
    
    private XMLReader() throws IOException, ParserConfigurationException, SAXException {
        
        URL smartpostdata = null;
        try {
            smartpostdata = new URL("http://smartpost.ee/fi_apt.xml");
        } catch (MalformedURLException ex) {
            Logger.getLogger(XMLReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(smartpostdata.openStream()));
        } catch (IOException ex) {
            Logger.getLogger(XMLReader.class.getName()).log(Level.SEVERE, null, ex);
        }
        String data = "";
        String line;
        
        while ((line = br.readLine()) != null) {
            data += line + "\n";
        }
        
        DocumentBuilderFactory docbuilder = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = docbuilder.newDocumentBuilder();
        document = db.parse(new InputSource(new StringReader(data)));
        document.getDocumentElement().normalize();
        NodeList Nodes = document.getElementsByTagName("place");
        
        for (int i = 0; i < Nodes.getLength(); i++) {
            Node node = Nodes.item(i);
            Element e = (Element) node;
            
            smartPost = new SmartPost(getValue("code", e), getValue("city", e).toUpperCase(), 
                    getValue("address", e), getValue("availability", e), 
                    getValue("postoffice", e), getValue("lat", e), 
                    getValue("lng", e), i);
            
            smartPosts.addSmartPost(smartPost);
            smartPosts.addCity(getValue("city", e).toUpperCase());
        }
    }
    
    private String getValue(String tag, Element e) {
        return e.getElementsByTagName(tag).item(0).getTextContent();
    }

    static public XMLReader runXMLReader() {
        if (XMLR == null) {
            try {
                XMLR = new XMLReader();
            } catch (IOException | ParserConfigurationException | SAXException ex) {
                Logger.getLogger(XMLReader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return XMLR;        
    }
}