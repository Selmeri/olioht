/*
 * Elmer Häyrynen, op.nro 0455933, c 2017
 */
package timo;

public class Cuddlebear extends Product {
    public Cuddlebear(){
        super();
        name = "Cuddlebear";
        fragile = false;
        dimension.put("weight", 1.0);
        dimension.put("height", 110.0);
        dimension.put("width", 120.0);
        dimension.put("depth", 100.0);
    }
    
}