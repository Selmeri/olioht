/*
 * Elmer Häyrynen, op.nro 0455933, c 2017
 */
package timo;

/**
 * Class to implement PackageType2, small size, semi-fast delivery, no chance of breakage, no distance limitations.
 * 
 */
public class PackageType2 extends Package {
    public PackageType2() {
        super();
        limitations.put("distance", 2000.0);
        limitations.put("weight", 5.0);
        limitations.put("height", 25.0);
        limitations.put("width", 25.0);
        limitations.put("depth", 25.0);
        packageType = 2;
        fragility = 0;
    }
}
