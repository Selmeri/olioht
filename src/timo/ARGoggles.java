/*
 * Elmer Häyrynen, op.nro 0455933, c 2017
 */
package timo;

public class ARGoggles extends Product {
    public ARGoggles(){
        super();
        name = "ARGoggles";
        fragile = true;
        fragility = 20;
        dimension.put("weight", 0.2);
        dimension.put("height", 15.0);
        dimension.put("width", 15.0);
        dimension.put("depth", 15.0);
    }
    
}
