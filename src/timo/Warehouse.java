/*
 * Elmer Häyrynen, op.nro 0455933, c 2017
 */
package timo;
import java.util.ArrayList;

/**
 * Warehouse class, initializes a warehouse, which holds set packages as an ArrayList
 * Includes methods for handling the contents of the warehouse (add, delete and get).
 */
public class Warehouse {
    static Warehouse warehouse = null;
    static ArrayList<Package> packages = null;
    
    private Warehouse() {
        packages = new ArrayList<>();
    }
    
    static public Warehouse getInstance() {
        if (warehouse == null) {
            warehouse = new Warehouse();
        }
        return warehouse;
    }

    public void addPackage(Package p) {
        packages.add(p);
    }
    
    public void deletePackage(Package p) {
        packages.remove(p);
    }
    static public ArrayList<Package> getPackages() {
        return packages;
    }
}
