/*
 * Elmer Häyrynen, op.nro 0455933, c 2017
 */
package timo;

/**
 * Class to handle all SmartPost locations in coordinates
 * 
 */

public class Coordinates {
    private final String latitude;
    private final String longtitude;
    
    public Coordinates(String a, String b) {
        latitude = a;
        longtitude = b;
    }
    
    public double getLatitude() {
        return Double.parseDouble(latitude);
    }
    
    public double getLongtitude() {
        return Double.parseDouble(longtitude);
    }
}
