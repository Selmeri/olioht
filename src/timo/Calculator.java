/*
 * Elmer Häyrynen, op.nro 0455933, c 2017
 */
package timo;

public class Calculator extends Product {
    public Calculator(){
        super();
        name = "Calculator";
        fragile = true;
        fragility = 60;
        dimension.put("weight", 0.2);
        dimension.put("height", 30.0);
        dimension.put("width", 20.0);
        dimension.put("depth", 10.0);
    }
    
}
