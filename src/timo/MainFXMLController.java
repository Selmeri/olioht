/*
 * Elmer Häyrynen, op.nro 0455933, c 2017
 */
package timo;

import java.util.Arrays;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;

public class MainFXMLController implements Initializable {
    
    //initializing few variables and all the FXML elements
    
    private Product item = null;
    private ArrayList<SmartPost> SPList;
    private final DataBuilder smartPosts = DataBuilder.getInstance();
    private final Warehouse warehouse = Warehouse.getInstance();
    
    @FXML
    private Label label;
    
    @FXML
    private WebView web;
    
    @FXML
    private ComboBox<String> selectAutomaton;
    
    @FXML
    private ComboBox<String> selectObject;
    
    @FXML
    private ComboBox<String> selectStart;
    
    @FXML
    private ComboBox<String> selectEnd;
    
    @FXML
    private ComboBox<String> selectClass;
    
    @FXML
    private ComboBox<Package> selectPackage;
    
    @FXML
    private ComboBox<SmartPost> startAutomata;
    
    @FXML
    private ComboBox<SmartPost> endAutomata;
    
    @FXML
    private Button createPackage;
    
    @FXML
    private Button sendPackage;
    
    @FXML
    private Button removeRoutes;
    
    @FXML
    private Button addAutomaton;
    
    @FXML
    private TextField weightBox;
    
    @FXML
    private TextField lengthBox;
    
    @FXML
    private TextField widthBox;
    
    @FXML
    private TextField depthBox;
    
    @FXML
    private TextField itemBox;
    
    @FXML
    private CheckBox fragileBox;
    
    
    @Override
    /**
    *Initializes the GUI:s components
    */
    public void initialize(URL url, ResourceBundle rb) {
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());
        XMLReader xmlr = XMLReader.runXMLReader();
        selectAutomaton.getItems().addAll(smartPosts.getCities());
        selectAutomaton.getSelectionModel().selectFirst();
        selectObject.getItems().addAll(Arrays.asList("ARGoggles","Calculator","Anvil","Cuddlebear","Paketoi oma tuote"));
        selectClass.getItems().addAll(Arrays.asList("1. luokka", "2. luokka", "3. luokka"));
        selectClass.getSelectionModel().selectFirst();
        selectStart.getItems().addAll(smartPosts.getCities());
        selectEnd.getItems().addAll(smartPosts.getCities());
        startAutomata.setDisable(true);
        endAutomata.setDisable(true);  
        sendPackage.setDisable(true);
        selectPackage.setDisable(true);
        fragileBox.setVisible(false);
        depthBox.setVisible(false);
        widthBox.setVisible(false);
        itemBox.setVisible(false);
        lengthBox.setVisible(false);
        weightBox.setVisible(false);
        label.setText("Valittu pakettiluokka 1, Pikalähetys, max 150km, ei rikkoutuville lähetyksille.");
    }
    
    
    
    @FXML
    /**
    *Adds the smart posts of a selected city to the map using the provided API
    */
    private void handleAutomatonAction(ActionEvent event) {
        String name = selectAutomaton.getValue();
        smartPosts.getTargetSmartPosts(name).forEach((sPost) -> {
            String location = "" + sPost.getAddress() + " " + sPost.getZIP() + " " + sPost.getCity() + "";
            String open = "" + sPost.getPostoffice() + ", Avoinna: " + sPost.getOpen()+"";
            web.getEngine().executeScript("document.goToLocation('" + location + "','" + open + "', 'blue')");
        });
    }
    
    @FXML
    /**
     * Checks which item user selects, and creates it from Product subclass OR from user definition.
     * 
     */
    private void objectsComboAction(ActionEvent event) {
        switch (selectObject.getValue()) {
            case "ARGoggles":
                item = new ARGoggles();
                break;
            case "Calculator":
                item = new Calculator();
                break;
            case "Anvil":
                item = new Anvil();
                break;
            case "Cuddlebear":
                item = new Cuddlebear();
                break;
            case "Paketoi oma tuote":
                item = null;
                break;
        }
        if ("Paketoi oma tuote".equals(selectObject.getValue())){
                fragileBox.setVisible(true);
                depthBox.setVisible(true);
                widthBox.setVisible(true);
                lengthBox.setVisible(true);
                weightBox.setVisible(true);
                itemBox.setVisible(true);
                label.setText("Anna lähetyksesi tiedot ylläoleviin kenttiin");
        } else {
            fragileBox.setVisible(false);
            depthBox.setVisible(false);
            widthBox.setVisible(false);
            lengthBox.setVisible(false);
            weightBox.setVisible(false);
            itemBox.setVisible(false);
        }
    }
    
    /**
     * Checks if the input from user is valid for creating a parcel
     * 
     */
    private Product testUserProduct() {
        boolean ok = true;
        String itemLength= lengthBox.getText();
        String itemWidth= widthBox.getText();
        String itemDepth= depthBox.getText();
        String itemName = itemBox.getText();
        String itemWeight = weightBox.getText();
        
        if (itemName.equals("")){
            ok=false;
        }
        if (!isDouble(itemLength) && !isDouble(itemWidth) && !isDouble(itemDepth) && !isDouble(itemWeight)){
            ok=false;
        }
        
        if (ok==true){
            return new Product(Double.parseDouble(itemWeight), Double.parseDouble(itemLength), 
            Double.parseDouble(itemWidth), Double.parseDouble(itemDepth),itemName,fragileBox.isSelected());
        } else { 
            return null;
        }
}
    
    private boolean isDouble(String value) {
        try {
            double db = Double.parseDouble(value);
        } catch (NumberFormatException error) {
            return false;
        }
        return true;
    }
    
    @FXML
    /**
     * Gets all smart posts of selected start city, also enables the selection combobox.
     */
    private void SelectStartAction(ActionEvent event) {
        startAutomata.getItems().clear();
        String place = selectStart.getValue();
        smartPosts.getTargetSmartPosts(place).forEach((sPost) -> {
            startAutomata.getItems().add(sPost);
        });

        if (startAutomata.isDisable()) {
            startAutomata.setDisable(false);
        }
        startAutomata.getSelectionModel().selectFirst();
    }
    
    @FXML
    /**
     * Gets all smart posts of selected destination city, also enables the selection combobox.
     */
    private void SelectEndAction(ActionEvent event) {
        endAutomata.getItems().clear();
        String place = selectEnd.getValue();
        smartPosts.getTargetSmartPosts(place).forEach((sPost) -> {
            endAutomata.getItems().add(sPost);
        });

        if (endAutomata.isDisable()) {
            endAutomata.setDisable(false);
        }
        endAutomata.getSelectionModel().selectFirst();
    }
    
    /**
     * Updates the contents of SelectPackage combobox, based on warehouse. Also enables the selection and sending of packages.
     */
    private void updateSelectPackage() {
        selectPackage.getItems().clear();
        selectPackage.getItems().addAll(Warehouse.getPackages());
        selectPackage.getSelectionModel().selectFirst();
        
        if (selectPackage.isDisable()) {
            selectPackage.setDisable(false);
        }
        
        if (sendPackage.isDisable()) {
            sendPackage.setDisable(false);
        }
        
        if (selectPackage.getValue()==null){
            selectPackage.setDisable(true);
            sendPackage.setDisable(true);
        }
    }
    
    @FXML
    /**
     * Displaying package info on selection
     */
    private void packageClassAction(ActionEvent event) {
        switch (selectClass.getValue()) {
            case "1. luokka":
                label.setText("Valittu pakettiluokka 1, Pikalähetys, max 150km, ei rikkoutuville lähetyksille.");
                break;
            case "2. luokka":
                label.setText("Valittu pakettiluokka 2, Turvalähetys, suositellaan rikkoutuville lähetyksille.");
                break;
            case "3. luokka":
                label.setText("Valittu pakettiluokka 3, Suuri lähetys, ei rikkoutuville lähetyksille.");
                break;
            }
    }
    
    @FXML
    /**
     * Package Creation, chooses the package for selected item, defines package class, runs dim and weight check function, creates package of all ok.
     */
    private void createButtonAction(ActionEvent event) {
        Package pack = null;
        boolean validPackage = true;
        
        if (item == null){
            item = testUserProduct();
        }
        
        String packGrade = selectClass.getValue();
        
        //defines the package type
        
        switch (packGrade) {
            case "1. luokka":
                pack = new PackageType1();
                break;
            case "2. luokka":
                pack = new PackageType2();
                break;
            case "3. luokka":
                pack = new PackageType3();
                break;
            }
        
            //testing origin and destination SmartPosts
        
            if (startAutomata.getSelectionModel().isEmpty()){
                label.setText("Valitse lähtöautomaatti!");
                validPackage = false;
            }
            
            if (endAutomata.getSelectionModel().isEmpty()){
                label.setText("Valitse kohdeautomaatti!");
                validPackage = false;
            }
            
            if (item!=null && validPackage == true){
        
                SmartPost start = startAutomata.getValue();
                SmartPost end = endAutomata.getValue();
            
                pack.setOrigin(start.getId());
                pack.setDestination(end.getId());
                pack.setItem(item);
            
                //Travel distance dimension test
            
                SmartPost startPost = DataBuilder.getInstance().getSmartPost(pack.getOrigin());
                SmartPost endPost = DataBuilder.getInstance().getSmartPost(pack.getDestination());
            
                //initializing the start and destination SmartPosts to Array list for using the API
            
                ArrayList<Double> array = new ArrayList();
                array.add(startPost.getLatitude());
                array.add(startPost.getLongtitude());
                array.add(endPost.getLatitude());
                array.add(endPost.getLongtitude());
            
                //Running the api call, which returns the distance, also running the deletePaths, since the first api call would otherwise draw the path to the map
            
                Double dist = (Double) web.getEngine().executeScript("document.createPath(" + array + ", 'white', " + pack.getPackageType() + ")");
                web.getEngine().executeScript("document.deletePaths()");
            
                if (dist > 150 && pack.getPackageType() == 1){
                    label.setText("1. luokan paketti voi kulkea vain 150km! Vaihda luokkaa!");
                    validPackage=false;
                } else {
                }
            
                //checks if the item dimensions/weight exceed package limitations
            
                if (testDimensions(item, pack)==true){
                } else {
                    validPackage = false;
            }
            
             //creates package to warehouse
            
            if (validPackage == true){
                Warehouse.warehouse.addPackage(pack);
                label.setText("Paketin luonti onnistui!");
                updateSelectPackage();
            }
        } else{
            label.setText("Paketin luonti ei onnistunut, tarkista tiedot.");
        }
    }
    
    @FXML
    /**
     *A check function to see if item dimensions and weight fit the package limitations.
     */
    private boolean testDimensions(Product item, Package pack) {
        boolean validDimension = false;
        boolean validWeight;
        double itemWeight = item.getDimension().get("weight");
        double packWeight = pack.getLimitations().get("weight");
        
        //initializing Array lists from item and package data hashmaps.
        
        ArrayList<Double> itemSize = new ArrayList();
        ArrayList<Double> packSize = new ArrayList();
        
        packSize.add(pack.getLimitations().get("height"));
        packSize.add(pack.getLimitations().get("width"));
        packSize.add(pack.getLimitations().get("depth"));
        
        itemSize.add(item.getDimension().get("height"));
        itemSize.add(item.getDimension().get("width"));
        itemSize.add(item.getDimension().get("depth"));
        
        //Checks if item dimensions exceed the package limit
        
        for (int i = 0; i < 3; i++) {
            if (packSize.get(i) < itemSize.get(i)) {
                label.setText("Tuote on liian suuri, vaihda pakettiluokkaa!");
                validDimension = false;
                break;
            } else {
                validDimension = true;
            }
        }
        
        //Checks if item weight exceeds the package limit
        
        if (itemWeight > packWeight) {
            label.setText("Tuote on liian painava, vaihda pakettiluokkaa!");
            validWeight = false;
        } else {
            validWeight = true;
        }
        
        if (validDimension && validWeight) {
            return true;
        } else {
            return false;
        }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
    }
    
    @FXML
    /**
     * Send button function, gets the instances of start and end post, displays the travel via API, and handles the package removal from warehouse 
     */
    private void sendButtonAction(ActionEvent event){
        Package pack = selectPackage.getValue();
        
        SmartPost startPost = DataBuilder.getInstance().getSmartPost(pack.getOrigin());
        SmartPost endPost = DataBuilder.getInstance().getSmartPost(pack.getDestination());
        
        String startlocation = "" + startPost.getAddress() + " " + startPost.getZIP() + " " + startPost.getCity() + "";
        String startopen = "" + startPost.getPostoffice() + ", Avoinna: " + startPost.getOpen()+"";
        
        String endlocation = "" + endPost.getAddress() + " " + endPost.getZIP() + " " + endPost.getCity() + "";
        String endopen = "" + endPost.getPostoffice() + ", Avoinna: " + endPost.getOpen()+"";
        
        //Sets lat and long of both posts into Arraylist for usage with provided API
        
        ArrayList<Double> array = new ArrayList();
        array.add(startPost.getLatitude());
        array.add(startPost.getLongtitude());
        array.add(endPost.getLatitude());
        array.add(endPost.getLongtitude());
        
        //Displays both the start and end smartpost, as well as draws the route via API
        
        web.getEngine().executeScript("document.goToLocation('" + startlocation + "','" + startopen + "', 'blue')");
        web.getEngine().executeScript("document.goToLocation('" + endlocation + "','" + endopen + "', 'blue')");
        web.getEngine().executeScript("document.createPath(" + array + ", 'blue', " + pack.getPackageType() + ")");
        
        //checks if item broke during transportation, and updates label accordingly
        
        if (pack.getItem().isFragile() && (pack.getItem().getFragility()< pack.getFragility())){
            label.setText("Paketin sisältö hajosi kuljetuksessa!");
        } else {
            label.setText("Paketin sisältö kesti kuljetuksen!");
        }
        
        //Deletes package from warehouse, updates the GUI element accordingly
        
        warehouse.deletePackage(pack);
        selectPackage.getSelectionModel().clearSelection();
        selectPackage.getItems().clear();
        updateSelectPackage();
    }
    
    @FXML
    /**
     * API call for removing all routes from the map and displaying info of succesful removal
     */
    private void removeRouteAction(ActionEvent event) {
        web.getEngine().executeScript("document.deletePaths()");
        label.setText("Reitit poistettiin karttanäkymästä!");
    }
}
