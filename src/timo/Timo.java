/*
 * Elmer Häyrynen, op.nro 0455933, c 2017
 */
package timo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Main class to launch the FXML-based ui and to initialize the controller to run the program.
 * 
 */
public class Timo extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("MainFXML.fxml"));
        Parent root = loader.load();
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments (none)
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
