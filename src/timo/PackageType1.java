/*
 * Elmer Häyrynen, op.nro 0455933, c 2017
 */
package timo;

/**
 * Class to implement PackageType1, medium size, fast delivery, high chance of breakage, distance limitation 150
 * 
 */
public class PackageType1 extends Package {
    public PackageType1() {
        super();
        limitations.put("distance", 150.0);
        limitations.put("weight", 10.0);
        limitations.put("height", 60.0);
        limitations.put("width", 60.0);
        limitations.put("depth", 30.0);
        packageType = 1;
        fragility = 50;
    }
}
