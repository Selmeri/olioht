/*
 * Elmer Häyrynen, op.nro 0455933, c 2017
 */
package timo;

/**
 * Class to implement PackageType3, large size, slow delivery, high chance of breakage, no distance limitation.
 * 
 */
public class PackageType3 extends Package {
    public PackageType3() {
        super();
        limitations.put("distance", 2000.0);
        limitations.put("weight", 250.0);
        limitations.put("height", 200.0);
        limitations.put("width", 150.0);
        limitations.put("depth", 150.0);
        packageType = 3;
        fragility = 90;
    }
    
}