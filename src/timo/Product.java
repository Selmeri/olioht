/*
 * Elmer Häyrynen, op.nro 0455933, c 2017
 */
package timo;
import java.util.HashMap;
import java.util.Map;

/**
 * Class for products, includes all product specific attributes, also implements the product Subclasses.
 * 
 */

public class Product {
    protected String name;
    protected boolean fragile;
    protected int fragility;
    protected Map<String, Double> dimension = new HashMap<>();
    
    public Product() {
        dimension.put("weight", 0.0);
        dimension.put("height", 0.0);
        dimension.put("width", 0.0);
        dimension.put("depth", 0.0);
    }
    
    public Product(Double a, Double b, Double c, Double d, String e, boolean f) {
        name = e;
        dimension.put("weight", a);
        dimension.put("height", b);
        dimension.put("width", c);
        dimension.put("depth", d);
        fragile = f;
        if (f) {
            fragility = 50;
        }
    }
    
    public boolean isFragile() {
        return fragile;
    }

    public int getFragility() {
        return fragility;
    }
    
    public String getName() {
        return name;
    }

    public Map<String, Double> getDimension() {
        return dimension;
    }
}