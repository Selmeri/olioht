/*
 * Elmer Häyrynen, op.nro 0455933, c 2017
 */
package timo;

import java.util.ArrayList;
import java.util.Collections;

public class DataBuilder {
    static private DataBuilder build = null;
    private final ArrayList<String> cities = new ArrayList();
    private final ArrayList<SmartPost> smartPosts = new ArrayList();
    
    private DataBuilder() {
    }
    
    static public DataBuilder getInstance() {
        if (build == null) {
            build = new DataBuilder();
        }
        return build;
    }
    
    public void addCity(String s) {
        if (!(cities.contains(s))) {
            cities.add(s);
            Collections.sort(cities);
        }
    }
    
    public void addSmartPost(SmartPost a) {
        smartPosts.add(a);
    }
    
    public ArrayList<String> getCities() {
        return cities;
    }
    
    public ArrayList<SmartPost> getSmartPosts() {
        return smartPosts;
    }
    
    public SmartPost getSmartPost(int i) {
        return smartPosts.get(i);
    }
    /**
    /Prints out all the smart posts of selected city, filtering by city name.
     * @param name
     * @return smartposts
    */
    public ArrayList<SmartPost> getTargetSmartPosts(String name) {
        ArrayList<SmartPost> sPosts = new ArrayList();
        smartPosts.stream().filter((sPost) -> (sPost.getCity().equals(name))).forEachOrdered((sPost) -> {
            sPosts.add(sPost);
        });
        
        return sPosts;
    }
}
