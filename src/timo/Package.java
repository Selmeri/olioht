/*
 * Elmer Häyrynen, op.nro 0455933, c 2017
 */
package timo;

import java.util.HashMap;
import java.util.Map;

/**
 * Class for handling packages. Includes all the attributes required for a package
 * including the implementation of the selected Product, as well as the package limitations, origin and destination.
 * In methods focuses on the ability to set values based on selected package class and selected smartPosts, and the selected item.
 */
abstract public class Package {
    protected Product item;
    protected int fragility;
    protected int packageType;
    protected int origin;
    protected int destination;
    protected Map<String, Double> limitations = new HashMap<>();
    
    public Package() {
        limitations.put("height", 0.0);
        limitations.put("width", 0.0);
        limitations.put("depth", 0.0);
        limitations.put("weight", 0.0);
        limitations.put("distance", 0.0);
    }
    
    @Override
    public String toString() {
        String a = item.getName();
        String b = ( a + " (" + packageType + " luokan lähetys)");
        return b;
    }
    
    public Map<String, Double> getLimitations() {
        return limitations;
    }
    
    public int getPackageType() {
        return packageType;
    }
    
    public Product getItem() {
        return item;
    }

    public int getFragility() {
        return fragility;
    }

    public int getOrigin() {
        return origin;
    }

    public int getDestination() {
        return destination;
    }
    
    public void setPackageType(int packageType) {
        this.packageType = packageType;
    }

    public void setItem(Product item) {
        this.item = item;
    }
    
    public void setFragility(int fragility) {
        this.fragility = fragility;
    }

    public void setOrigin(int origin) {
        this.origin = origin;
    }

    public void setDestination(int destination) {
        this.destination = destination;
    }
}
